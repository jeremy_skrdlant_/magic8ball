//
//  ContentView.swift
//  Magic8Ball
//
//  Created by Jeremy Skrdlant on 6/1/20.
//  Copyright © 2020 NWKTC. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        Text("Hello, Jeremy!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
